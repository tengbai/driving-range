package com.self.tt.util;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class LambdaTest {


	@Test
	public void lambda_could_visit_outside_param_without_final() {
		List<String> list = Arrays.asList("page", "join");
		String hello = "Hello";
		List<Object> test = list.stream().map(name -> hello + name).collect(Collectors.toList());
		test.forEach(System.out::println);
	}


	@Test
	public void lambda_could_not_modify_outside_param_but_use_AtomicReference() {
		AtomicReference<String> name = new AtomicReference<>("page");
		List<String> actionList = Arrays.asList("jump", "stand");

		actionList.forEach(name::set);

		assertEquals("stand", name.get());
	}
}
