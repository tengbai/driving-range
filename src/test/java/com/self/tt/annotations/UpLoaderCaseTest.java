package com.self.tt.annotations;

import com.self.tt.domain.CountryInfo;
import com.self.tt.service.CountryTransfer;
import org.junit.Assert;
import org.junit.Test;

public class UpLoaderCaseTest {


	@Test
	public void should_return_false_when_compare_1_with_2() {
		Assert.assertFalse(false);
	}

	@Test
	public void should_transfer_country_to_upper_case_when_country_value_is_lower_case() {
		String country = "us";
		CountryInfo countryInfo = new CountryInfo(country, country);

		new CountryTransfer().transfer(countryInfo);

		Assert.assertEquals("US", countryInfo.getCountry());
	}

	@Test
	public void should_not_throw_exception_when_field_is_null() {
		CountryInfo countryInfo = new CountryInfo(null, "US");

		new CountryTransfer().transfer(countryInfo);

		Assert.assertNull(countryInfo.getCountry());
	}


}
