package com.self.tt.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@Slf4j
@RestController
@RequestMapping("/timeout")
public class TimeoutResource {

	@GetMapping("/test")
	public Object testTimeout(@RequestParam("time") long time) throws InterruptedException {

		long startTime = System.currentTimeMillis();
		Thread.sleep(time);

		log.info("request cost {}ms", (System.currentTimeMillis() - startTime));
		return new HashMap<>();
	}
}
