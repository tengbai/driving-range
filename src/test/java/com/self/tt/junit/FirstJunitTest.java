package com.self.tt.junit;

import com.self.tt.junit.customerize.FirstAnnotation;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;

import static org.junit.jupiter.api.Assertions.*;


@DisplayName("A first JUnit 5 test class.")
class FirstJunitTest {

	@Test
	@FirstAnnotation
	@DisplayName("should equals.")
	void myFirstTest() {
		assertEquals(2, 1 + 1);

		assertEquals(2, 2, "The optional assertion message is now the last parameter.");
	}


	@Test
	void lambdaTest() {
		assertAll("persion",
				() -> assertEquals("Aaron", "Aaron"),
				() -> assertEquals("Page", "Page"));
	}

	@Test
	void exceptionTest() {
		Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
			throw new IllegalArgumentException("a message");
		});

		assertEquals("a message", exception.getMessage());
	}


	@Test
	@EnabledOnOs(OS.MAC)
	void onlyTestOnMAC() {
		assertEquals("MAC", OS.MAC.toString());
	}
}
