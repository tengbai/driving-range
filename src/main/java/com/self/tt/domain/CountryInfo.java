package com.self.tt.domain;

import com.self.tt.annotations.UpperCase;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CountryInfo {

	@UpperCase
	private String country;

	@NonNull
	private String countryCode;
}
