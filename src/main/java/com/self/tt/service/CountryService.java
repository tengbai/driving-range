package com.self.tt.service;

import com.self.tt.domain.CountryInfo;
import org.springframework.stereotype.Service;

@Service
public class CountryService {


	public CountryInfo findByCountryCode(String countryCode) {
		return new CountryInfo(countryCode, countryCode);
	}
}
