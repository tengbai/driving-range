package com.self.tt.rest;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.self.tt.domain.CountryInfo;
import com.self.tt.service.CountryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CountryResource.class)
public class CountryResourceTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CountryService countryService;


	@Test
	public void should_response_200_when_request_country_info_by_country_code() throws Exception {
		String country = "us";
		CountryInfo contryInfo = new CountryInfo(country, "US");
		given(countryService.findByCountryCode(country)).willReturn(contryInfo);

		mockMvc.perform(
				get("/api/countries/" + country)
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andDo(print())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(content().json(new ObjectMapper().writeValueAsString(contryInfo)));
	}


	@Test
	public void should_request_false_when_some_perperty_was_set_non_null() throws Exception {

		mockMvc.perform(
				put("/api/countries/")
						.contentType(MediaType.APPLICATION_JSON)
						.content("{'country':'name', 'countryCode':null}"))
				.andExpect(status().is4xxClientError())
				.andDo(print());

	}


}
