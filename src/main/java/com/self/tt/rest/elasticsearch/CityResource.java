package com.self.tt.rest.elasticsearch;

import com.google.common.collect.Lists;
import com.self.tt.domain.elasticsearch.City;
import com.self.tt.service.elasticsearch.CityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Slf4j
@RestController
@RequestMapping("/api/cities")
public class CityResource {

	@Autowired
	private CityService cityService;

	@PostMapping
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void save(@RequestParam("name") String name) {
		log.info("name=" + name);
		cityService.save(name);
	}

	@GetMapping
	public List<City> findAll() {
		log.info("find all");
		return Lists.newArrayList(cityService.findAll());
	}

	@GetMapping("/{name}")
	public List<City> findByName(@PathVariable("name") String name) {
		log.info("find by name:{}", name);
		return cityService.findByName(name);
	}

}
