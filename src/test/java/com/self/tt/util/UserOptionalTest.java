package com.self.tt.util;

import com.self.tt.domain.User;
import com.self.tt.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;

public class UserOptionalTest {


	private UserService userService;


	@Before
	public void setUp() {
		this.userService = new UserService();
	}


	@Test
	public void should_return_default_value_when_user_is_not_but_company_is_null() {

		User user = new User();

		String company = userService.getCompanyUseOptional(user);

		Assert.assertThat(company, is("No Setting"));
	}


	@Test(expected = NullPointerException.class)
	public void should_throw_exception_when_user_is_null() {
		User user = null;

		userService.getCompanyUseOptional(user);
	}
}
