package com.self.tt.util;

import org.junit.Test;

import java.util.*;
import java.util.function.Consumer;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class JavaOptionalTest {


	@Test(expected = NullPointerException.class)
	public void use_optional_of_method() {

		String name = null;

		Optional.of(name);
	}


	@Test
	public void use_optional_ofNullable_when_value_is_null() {
		String name = null;

		Optional.ofNullable(name);
	}


	@Test
	public void use_option_ofNullable_when_value_is_not_null() {
		Optional.ofNullable(2);
	}

	@Test
	public void use_optional_empty_method() {
		Optional<Object> optional = Optional.empty();

		// Todo ： when use Optional.empty();
		System.out.println(optional);
	}


	@Test
	public void use_optional_isPersent() {
		Optional<Object> optional = Optional.ofNullable(null);
		Optional<String> name = Optional.ofNullable("Page");

		assertThat(optional.isPresent(), is(false));
		assertThat(name.isPresent(), is(true));
	}


	@Test
	public void use_optional_ifPresent_method() {
		Optional<String> name = Optional.ofNullable("Page");

		name.ifPresent(new Consumer<String>() {
			@Override
			public void accept(String name) {
				assertThat(name, is("Page"));
				System.out.println(name);
			}
		});
	}


	@Test
	public void use_optional_ofElse() {
		Optional<String> name = Optional.ofNullable(null);

		String finalName = name.orElse("Page");

		assertThat(finalName, is("Page"));
	}


	@Test
	public void use_optional_orElse() throws NullPointerException {
		Optional<String> name = Optional.ofNullable(null);

		// Todo: how to use.s
	}


	@Test
	public void use_optional_orElseGet() {
		Optional<String> name = Optional.ofNullable(null);

		String finalName = name.orElseGet(() -> "Page");

		assertThat(finalName, is("Page"));
	}


	@Test
	public void use_optional_filter() {
		List<String> nameList = Arrays.asList("A", "B", "C");
		Optional<List<String>> nameListOptional = Optional.ofNullable(nameList);

		Optional<List<String>> finalList = nameListOptional.filter(list -> list.size() == 3);

		assertThat(finalList.isPresent(), is(true));
		assertThat(finalList.get().size(), is(3));
		assertThat(finalList.get().get(0), is("A"));
		assertThat(finalList.get().get(1), is("B"));
		assertThat(finalList.get().get(2), is("C"));
	}


	/**
	 * 不能够在map中对list 直接进行操作,例如设定新的值
	 */
	@Test
	public void use_optional_map() {
		List<String> nameList = Collections.singletonList("A");
		Optional<List<String>> nameListOption = Optional.ofNullable(nameList);

		Optional<List<String>> finalNameListOptional = nameListOption.map((list) -> {
			List<String> newList = new ArrayList<>();
			newList.addAll(list);
			newList.add("B");
			return newList;
		});

		assertThat(finalNameListOptional.isPresent(), is(true));
		assertThat(finalNameListOptional.get().size(), is(2));
		assertThat(finalNameListOptional.get().get(0), is("A"));
		assertThat(finalNameListOptional.get().get(1), is("B"));
	}


	@Test
	public void use_optional_flagMap() {
		Optional<String> address = Optional.ofNullable("beijing");

		Optional<String> finalAddress = address.flatMap((addressTemp) -> Optional.of(addressTemp + " changping"))
				.flatMap((addressTemp) -> Optional.of(addressTemp + " beiqijia"))
				.map((addressTemp) -> addressTemp + ".");

		assertThat(finalAddress.get(), is("beijing changping beiqijia."));
	}


	@Test(expected = NullPointerException.class)
	public void use_optional_to_check_null() {

		List<String> nameList = null;

		Optional.ofNullable(nameList)
				.map(list -> new ArrayList())
				.orElseThrow(() -> new NullPointerException());
	}


	@Test
	public void use_optional_to_check_null_and_return_empty_string() {
		String name = null;

//		String finalName = Optional.ofNullable(name)
//				.map((value) -> {
//					return name + "ABC";
//				}).orElse("EMPTY");

		String finalName = null == name ? "EMPTY" : name + "ABc";

		assertThat(finalName, is("EMPTY"));
	}

}

