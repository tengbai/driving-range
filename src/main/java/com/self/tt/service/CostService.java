package com.self.tt.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CostService {

	private String buildSomething() throws InterruptedException {
		Thread.sleep(1000 * 1);

		return "A";
	}

	@Async
	public List<Future<String>> build(List<String> list) {
		List<Future<String>> return_null = list.stream()
				.map(value -> {
					try {
						return new AsyncResult<>(buildSomething());
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					log.info("return null");
					return null;
				})
				.collect(Collectors.toList());
		log.info("list is null :{}", Objects.isNull(return_null));

		return return_null;
	}
}
