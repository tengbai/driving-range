package com.self.tt.repository.elasticsearch;

import com.self.tt.domain.elasticsearch.City;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CityRepository extends ElasticsearchRepository<City, String> {
	List<City> findByName(String name);
}
