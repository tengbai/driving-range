package com.self.tt.service.elasticsearch;

import com.self.tt.domain.elasticsearch.City;
import com.self.tt.repository.elasticsearch.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityService {

	@Autowired
	private CityRepository cityRepository;


	public void save(String name) {
		cityRepository.save(City.builder().name(name).build());
	}


	public Iterable<City> findAll() {
		return cityRepository.findAll();
	}

	public List<City> findByName(String name) {
		return cityRepository.findByName(name);
	}
}
