package com.self.tt.rest;

import com.self.tt.service.CostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

@Slf4j
@RestController
@RequestMapping("/api/cost")
public class CostResource {

	@Autowired
	private CostService costService;

	@GetMapping
	public List<String> get() {
		List<String> list = Arrays.asList("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");

		log.info("list.size={}", list.size());
		long start = new Date().getTime();
		List<Future<String>> resultList = costService.build(list);
		long end = new Date().getTime();

		log.info("cost time is {}", (end - start));
//		log.info("result.size={}", resultList.size());

		return Collections.emptyList();
	}
}
