package com.self.tt.junit;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

public class HamcrestTest {

	@Test
	@DisplayName("use assertThat() is() and EqualTo() in Hamcrest.")
	void use_assertThat_is_equalTo() {
		assertThat(1 + 1, is(equalTo(2)));
	}

}
