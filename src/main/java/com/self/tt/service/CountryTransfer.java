package com.self.tt.service;

import com.self.tt.annotations.UpperCase;
import com.self.tt.domain.CountryInfo;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class CountryTransfer {


	public void transfer(CountryInfo countryInfo) {

		Field[] fields = CountryInfo.class.getDeclaredFields();

		List<Field> fieldList = Arrays.asList(fields);

		fieldList.forEach(field -> {
			if (field.isAnnotationPresent(UpperCase.class)) {
				if (Objects.nonNull(countryInfo.getCountry())) {
					System.out.println(countryInfo.getCountry());
					countryInfo.setCountry(countryInfo.getCountry().toUpperCase());
				}
			}
		});
	}


}
