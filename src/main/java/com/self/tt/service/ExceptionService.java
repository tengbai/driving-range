package com.self.tt.service;


import com.self.tt.exception.NotFoundException;
import com.self.tt.exception.NotFoundException2;

public class ExceptionService {


	public String testNotFoundException(String name) throws NotFoundException {
		if ("Page".equals(name)) {
			return name;
		}

		throw new NotFoundException("Not Found");
	}


	public String testNotFoundException2(String name) {
		if ("Page".equals(name)) {
			return name;
		}

		throw new NotFoundException2("Not Found 2");
	}
}
