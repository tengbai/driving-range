package com.self.tt;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
@Configuration
@EnableElasticsearchRepositories("com.self.tt")
public class DrivingRangeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DrivingRangeApplication.class, args);
	}
}
