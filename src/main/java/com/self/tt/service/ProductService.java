package com.self.tt.service;

import com.self.tt.exception.NotFoundException;
import com.self.tt.rest.dto.ProductDTO;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

	public ProductDTO query(String productId, String countryCode) throws NotFoundException {
		if ("ABC".equals(productId) && "US".equals(countryCode)) {
			return new ProductDTO("ABC", "US", "Super ABC");
		}
		throw new NotFoundException("Not found");
	}
}
