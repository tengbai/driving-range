package com.self.tt.rest;

import com.self.tt.domain.CountryInfo;
import com.self.tt.service.CountryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/countries")
public class CountryResource {

	@Autowired
	private CountryService countryService;


	@GetMapping("/{countryCode}")
	public CountryInfo get(@PathVariable("countryCode") String countryCode) {
		log.info("countryCode={}", countryCode);
		return countryService.findByCountryCode(countryCode);
	}


	@PutMapping("/{countryCode}")
	public CountryInfo update(@PathVariable("countryCode") String countryCode,
	                          @RequestBody CountryInfo countryInfo) {
		log.info("countryCode={}", countryCode);
		return countryInfo;
	}
}
