package com.self.tt.java8.lambda;

import com.self.tt.java8.interfaces.TestInterface;

public class LambdaTest {

	public static void main(String[] args) {
		minus(100, 40, (int a, int b) -> a - b);
	}

	public static void minus(int a, int b, TestInterface testInterface) {
		int result = testInterface.minus(a, b);
		System.out.println(a + "-" + b + "=" + result);
	}

}
