package com.self.tt.service;

import com.self.tt.exception.NotFoundException;
import com.self.tt.rest.dto.ProductDTO;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

	@InjectMocks
	private ProductService productService;

	@Rule
	public ExpectedException thrown = ExpectedException.none();


	@Test
	public void should_return_product_dto_when_product_id_is_ABC_and_country_code_is_US() throws NotFoundException {
		ProductDTO productDTO = productService.query("ABC", "US");

		assertThat(productDTO.getName(), is("ABC"));
		assertThat(productDTO.getCountryCode(), is("US"));
		assertThat(productDTO.getDescription(), is("Super ABC"));
	}

	@Test
	public void should_throw_NotFoundException_when_product_id_is_empty_and_country_code_is_empty() throws NotFoundException {
		thrown.expect(NotFoundException.class);
		thrown.expectMessage("Not found");

		productService.query("", "");
	}

}