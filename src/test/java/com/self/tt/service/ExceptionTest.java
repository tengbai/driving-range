package com.self.tt.service;

import com.self.tt.exception.NotFoundException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(MockitoJUnitRunner.class)
public class ExceptionTest {

	@InjectMocks
	private ProductService productService;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test(expected = NotFoundException.class)
	public void use_test_annotation_to_catch_expect_exception() throws NotFoundException {
		productService.query("", "");
	}


	@Test
	public void use_expect_exception_object_to_assert_exception() throws NotFoundException {
		thrown.expect(NotFoundException.class);
		thrown.expectMessage("Not found");

		productService.query("", "");
	}


	@Test
	public void use_junit5_assertThrows_to_assert_NotFoundException(){
		NotFoundException exception = assertThrows(NotFoundException.class, () -> {
			productService.query("", "");
		});

		assertThat(exception.getMessage(), is("Not found"));
	}
}
