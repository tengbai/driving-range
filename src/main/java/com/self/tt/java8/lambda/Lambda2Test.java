package com.self.tt.java8.lambda;

import com.self.tt.java8.interfaces.Test2Interface;

public class Lambda2Test {


	public static void main(String[] args) {
		String word = "hello";
		justDoIt(word, System.out::print);
	}


	public static void justDoIt(String word, Test2Interface test2Interface) {
		test2Interface.work(word);
	}

}
