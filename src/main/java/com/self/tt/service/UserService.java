package com.self.tt.service;

import com.self.tt.domain.User;

import java.util.Optional;

public class UserService {


	public String getCompanyUseOptional(User user) {

		Optional<User> userOp = Optional.ofNullable(user);

		userOp.orElseThrow(NullPointerException::new);

		return userOp.map(User::getCompany)
				.orElse("No Setting");
	}

	public String getCompany(User user) {

		if (null == user) {
			throw new NullPointerException();
		}

		return null == user.getCompany() ? "No Setting" : user.getCompany();
	}
}
