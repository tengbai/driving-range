package com.self.tt.junit.data;

import org.junit.platform.commons.util.StringUtils;

public class BomService {


	public String getMachineType(String material) {
		if (StringUtils.isBlank(material)) {
			throw new NullPointerException("material can not be null.");
		}
		return material.substring(0, 5);
	}
}
