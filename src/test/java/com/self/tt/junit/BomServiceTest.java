package com.self.tt.junit;

import com.self.tt.junit.data.BomService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BomServiceTest {

	private BomService bomService = new BomService();

	@Test
	void should_assert_exception() {

		Throwable exception = assertThrows(NullPointerException.class, () -> {
			bomService.getMachineType(null);
		});

		assertEquals("material can not be null.", exception.getMessage());
	}

	@Test
	void should_return_machind_type_and_length_is_5() {
		String machineType = bomService.getMachineType("AAAAACCCCC");

		assertEquals("AAAAA", machineType);
	}
}
