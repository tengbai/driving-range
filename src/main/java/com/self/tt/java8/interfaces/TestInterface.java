package com.self.tt.java8.interfaces;

@FunctionalInterface
public interface TestInterface {

	int minus(int a, int b);
}
